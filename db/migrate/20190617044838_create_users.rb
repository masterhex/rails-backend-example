class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :login
      t.string :node_id
      t.text :avatar_url
      t.string :gravatar_id
      t.text :url
      t.text :html_url
      t.text :followers_url
      t.text :following_url
      t.text :gists_url
      t.text :starred_url
      t.text :subscriptions_url
      t.text :organizations_url
      t.text :repos_url
      t.string :name
      t.string :company
      t.string :blog
      t.string :location
      t.string :email

      t.timestamps
    end
  end
end
