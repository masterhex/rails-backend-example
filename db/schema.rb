# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_06_17_044838) do

  create_table "users", force: :cascade do |t|
    t.string "login"
    t.string "node_id"
    t.text "avatar_url"
    t.string "gravatar_id"
    t.text "url"
    t.text "html_url"
    t.text "followers_url"
    t.text "following_url"
    t.text "gists_url"
    t.text "starred_url"
    t.text "subscriptions_url"
    t.text "organizations_url"
    t.text "repos_url"
    t.string "name"
    t.string "company"
    t.string "blog"
    t.string "location"
    t.string "email"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

end
