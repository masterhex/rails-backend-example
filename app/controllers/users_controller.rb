require 'net/http'
require 'uri'
require 'json'

class UsersController < ApplicationController
    def index
        @users = User.all
    end

    def signin
        @code = params[:code]
        @client_id = '6dd063c2da70bf5a1a58'
        @client_secret = '1b73ab325f4cbdbefb4b03d94b2406259b1f2b43'



        uri = URI.parse("https://github.com/login/oauth/access_token")
        params = { :code => @code, :client_id => @client_id, :client_secret => @client_secret  }
        uri.query = URI.encode_www_form( params )
        header = {'Accept': 'application/json'}


        # Create the HTTP objects
        http = Net::HTTP.new(uri.host, uri.port)
        request = Net::HTTP::Post.new(uri.request_uri, header)
        http.use_ssl = true
        # Send the request
        response = http.request(request)
        token = ActiveSupport::JSON.decode response.body
        userJson = get_user(token)
        
        @user = User.new
        map_user(@user, userJson)
        @user.save
    end

    private def get_user(token)
        uri = URI.parse("https://api.github.com/user")
        header = {'Authorization': 'token ' + token['access_token']}
        # Create the HTTP objects
        http = Net::HTTP.new(uri.host, uri.port)
        request = Net::HTTP::Get.new(uri.request_uri, header)
        http.use_ssl = true
        # Send the request
        response = http.request(request)
        # print response.body.message
        # user = JSON.parse(response.body, object_class: User, allow_nan: true, create_additions: false)
        user = ActiveSupport::JSON.decode response.body
    end

    private def map_user(user, userJson)
        user.login = userJson['login']
        user.node_id = userJson['node_id']
        user.avatar_url = userJson['avatar_url']
        user.gravatar_id = userJson['gravatar_id']
        user.url = userJson['url']
        user.html_url = userJson['html_url']
        user.followers_url = userJson['followers_url']
        user.following_url = userJson['following_url']
        user.gists_url = userJson['gists_url']
        user.starred_url = userJson['starred_url']
        user.subscriptions_url = userJson['subscriptions_url']
        user.organizations_url = userJson['organizations_url']
        user.repos_url = userJson['repos_url']
        user.name = userJson['name']
        user.company = userJson['company']
        user.blog = userJson['blog']
        user.location = userJson['location']
        user.email = userJson['email']
    end
end

